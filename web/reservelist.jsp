<%-- 
    Document   : reservelist
    Created on : Dec 7, 2014, 12:41:58 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>

<%
try {
        /* Create string of connection url within specified format with machine
        name, port number and database name. Here machine name id localhost and 
        database name is student. */
        String connectionURL = "jdbc:mysql://localhost:3306/hotel";
        // declare a connection by using Connection interface
        Connection connection = null;
        /* declare object of Statement interface that is used for executing sql 
        statements. */
        Statement statement = null;
        // declare a resultset that uses as a table for output data from tha table.
        ResultSet rs = null;
        // Load JBBC driver "com.mysql.jdbc.Driver"
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        /* Create a connection by using getConnection() method that takes parameters 
        of string type connection url, user name and password to connect to database.*/
        connection = DriverManager.getConnection(connectionURL, "root", "");
        /* createStatement() is used for create statement object that is used for 
        sending sql statements to the specified database. */
        statement = connection.createStatement();
        // sql query to retrieve values from the secified table.
        String QueryString = "SELECT * from reservation where user_id = 1";
        rs = statement.executeQuery(QueryString);
%>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reservation List</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>
  
  <body>
      <div class="navbar navbar-default">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#">Brand</a>
  </div>
  <div class="navbar-collapse collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Active</a></li>
      <li><a href="#">Link</a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li class="divider"></li>
          <li class="dropdown-header">Dropdown header</li>
          <li><a href="#">Separated link</a></li>
          <li><a href="#">One more separated link</a></li>
        </ul>
      </li>
    </ul>
    <form class="navbar-form navbar-left">
      <input type="text" class="form-control col-lg-8" placeholder="Search">
    </form>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#">Link</a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li class="divider"></li>
          <li><a href="#">Separated link</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>
      
    <h1>Hello, world!</h1>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    
    <div class="container">
        <div class="row">
            <div class="box">
                <div class="col-lg-12">
    
                    
    <table class="table table-striped table-hover table-bordered table-responsive">
        <thead>
            <tr>
                <th>Index</th>
                <th>Room Type</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Price</th>
                <th>Status</th>
                <th>Cancel Booking</th>
            </tr>
        </thead>
        <tbody>
            <%  int index = 0;
                while (rs.next()) 
                {
                String id = rs.getString(1);
                index = index +1;
                String room = null;
                if(rs.getString(2).equals("1"))
                {room = "Superior-Single";}
                else if(rs.getString(2).equals("2"))
                {room = "Superior-Double";}
                else if(rs.getString(2).equals("3"))
                {room = "Superior-Triple";}
                else if(rs.getString(2).equals("4"))
                {room = "Mini-Suite";}
                    
            %>
                <tr>
                    <td><%=index%></td>
                    <td><%=room%></td>
                    <td><%=rs.getDate(5)%></td>
                    <td><%=rs.getDate(6)%></td>
                    <td><%=rs.getInt(8)%></td>
                    <td><%=rs.getString(3)%></td>
                    <td><a href="managereservation.jsp?id=<%=id%>"><img src="cancel.png" height="25" width="30" class="img-center center-block"></a></td>  
                </tr>
            <% 
                } 
            %>
        </tbody>
    </table>
  
                </div>
            </div>
        </div>
    </div>
    
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

<%
        // close all the connections.
        rs.close();
        statement.close();
        connection.close();
        
        } 
        catch (Exception ex) {
        
            out.println("Unable to connect to database.");
            out.println(ex);
        }
        %>
