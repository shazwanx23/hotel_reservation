<%-- 
    Document   : cust_confirm_res.jsp
    Created on : Dec 6, 2014, 4:59:00 PM
    Author     : troubleshoot
--%>

<%@include file="db_connect/connect.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String reservation_id = request.getParameter("reservation_id");
    int reserve_id = Integer.parseInt(reservation_id);
    
    String sqlUpdate = "UPDATE reservation" +
                           " SET status = 'user_confirmed'" +
                           " WHERE reservation_id = " + reserve_id;

	boolean reservationConfirmed = jdbcUtility.jdbcUpdate(sqlUpdate, con);
	
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Confirmation</h1>
        <% 
        if (reservationConfirmed)
	{
            System.out.println("Reservation confirmed successfully");
            //String redirectURL = "http://customer_confirm.jsp";
            //response.sendRedirect(redirectURL);
	}
	else
	{
            System.out.println("Record updating failure");
            System.out.println("");
	}
        %>
    </body>
</html>
