<%-- 
    Document   : admin_confirm.jsp
    Created on : Dec 6, 2014, 5:15:54 PM
    Author     : troubleshoot
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="db_connect/connect.jsp" %>
<% 
      String sqlQuery = "SELECT * FROM reservation";
      ResultSet rs = null;
       try
        {
            Statement queryStmt = con.createStatement();
            rs = queryStmt.executeQuery(sqlQuery);
        }
        catch (SQLException ex)
        {
            while (ex != null)
            {
                System.out.println ("SQLState: " +
                                     ex.getSQLState ());
                System.out.println ("Message:  " +
                                     ex.getMessage ());
                System.out.println ("Vendor:   " +
                                     ex.getErrorCode ());
                ex = ex.getNextException ();
                System.out.println ("");
            }
        }
        catch (java.lang.Exception ex)
        {
            ex.printStackTrace ();
        }       

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>All Reservations</h1>
        <<table border='1' cellspacing='1' cellpadding='1'>>
            <tr>
            <th>Room ID</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Status</th>
            <th>Action</th>
            </tr>
            <% 
            while (rs.next()) 
            {    
                String reservation_id = rs.getString("reservation_id");
                String room_id = rs.getString("room_id");
                String start_date = rs.getString("start_date");
                String end_date = rs.getString("end_date");
                String status = rs.getString("status");
                
                out.println("  <tr>");
                out.println("    <td>&nbsp;" + room_id + "</td>");
                out.println("    <td>&nbsp;" + start_date + "</td>");
                out.println("    <td>&nbsp;" + end_date + "</td>");
                out.println("    <td>&nbsp;" + status + "</td>");
                if(status.equals("user_confirmed")){
                out.println("    <td><a href=\"admin_confirm_res.jsp?reservation_id="+ reservation_id +"\">confirm</a></td>");
                }
                out.println("  </tr>");
                
            }
            %>
        </table>
    </body>
</html>
